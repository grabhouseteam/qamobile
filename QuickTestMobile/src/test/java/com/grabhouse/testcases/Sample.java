package com.grabhouse.testcases;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.grabhouse.pages.LoginPage;
import com.grabhouse.utility.AppiumServer;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class Sample {

	AndroidDriver driver = null;
	LoginPage loginPage;
	AppiumServer service;
	DesiredCapabilities cap;
	String url;
	LoginPage loginpage;

	@BeforeClass
	public void setUp() {
		service = new AppiumServer();
		url = service.startAppiumServer();
		System.out.println("URL is : " + url);
		cap = new DesiredCapabilities();
		loginpage = new LoginPage(driver);
	}

	@BeforeMethod
	public void settingDesiredCapabilities() throws MalformedURLException {
		cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Android");
		cap.setCapability(MobileCapabilityType.APP_PACKAGE, "com.app.grabhouse");
		cap.setCapability(MobileCapabilityType.APP_ACTIVITY, "com.app.grabhouse.activity.LoginActivity_");
		driver = new AndroidDriver(new URL(url), cap);
	}

	@Test
	public void Test() throws InterruptedException {
		loginpage.loginWithFacebook();
	}

	@AfterClass
	public void tearDown() {
		service.stopAppiumServer();
		System.out.println("Server Stopped");
	}

}
