package com.grabhouse.utility;

import java.io.File;

import org.testng.Assert;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;

/**
 * This page models Appium server
 *
 */
public class AppiumServer {

	// e.g. in Windows
	String winAppiumInstallationDir = "C:/Program Files (x86)/Appium";
	// e.g. for Mac
	String iosAppiumInstallationDir = null /* "/Applications" */;

	AppiumDriverLocalService service = null;

	public AppiumServer() {
		File classPathRoot = new File(System.getProperty("user.dir"));
		String osName = System.getProperty("os.name");

		if (osName.contains("Windows")) {
			service = AppiumDriverLocalService.buildService(new AppiumServiceBuilder()
					.usingDriverExecutable(new File(winAppiumInstallationDir + File.separator + "node.exe"))
					.withAppiumJS(new File(winAppiumInstallationDir + File.separator + "node_modules" + File.separator
							+ "appium" + File.separator + "bin" + File.separator + "appium.js"))
					.withLogFile(
							new File(new File(classPathRoot, File.separator + "src/test/resources/logs"), "logs.txt")));

		} else if (osName.contains("Mac")) {
			service = AppiumDriverLocalService.buildService(new AppiumServiceBuilder()
					.usingDriverExecutable(
							new File(iosAppiumInstallationDir + "/Appium.app/Contents/Resources/node/bin/node"))
					.withAppiumJS(new File(iosAppiumInstallationDir
							+ "/Appium.app/Contents/Resources/node_modules/appium/bin/appium.js"))
					.withLogFile(
							new File(new File(classPathRoot, File.separator + "src/test/resources/logs"), "logs.txt")));

		} else {
			// you can add for other OS, just to track added a fail message
			Assert.fail("Starting appium is not supporting the current OS.");
		}

	}

	/**
	 * Starts appium server
	 */
	public String startAppiumServer() {
		if (service != null) {
			service.stop();
		}
		service.start();
		return service.getUrl().toString().trim();
	}

	/**
	 * Stops appium server
	 */
	public void stopAppiumServer() {
		service.stop();
	}

}