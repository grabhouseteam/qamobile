package com.grabhouse.pages;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class LoginPage {

	public AndroidDriver driver;

	public LoginPage(AndroidDriver driver) {
		this.driver = driver;
		loadElements();
	}

	public void loadElements() {

		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@AndroidFindBy(id = "com.app.grabhouse:id/login_button")
	public WebElement facebook;

	@AndroidFindBy(id = "com.app.grabhouse:id/googleSignIn")
	public WebElement google;

	@AndroidFindBy(id = "com.app.grabhouse:id/etMobile")
	public WebElement mobile_number;

	@AndroidFindBy(id = "com.app.grabhouse:id/sendButton")
	public WebElement login_button;

	@FindBy(id = "email")
	public WebElement facebook_email;

	@FindBy(id = "pass")
	public WebElement facebook_password;

	@FindBy(id = "loginbutton")
	public WebElement facebook_loginbutton;

	public void loginWithFacebook() throws InterruptedException {
		Thread.sleep(3000);
		facebook.click();
		Set<String> contexts = driver.getContextHandles();
		System.out.println(contexts);
		for (String context : contexts) {
			if (context.contains("WEBVIEW")) {
				driver.context(context);
				facebook_email.sendKeys("sandeepteja@hotmail.com");
				facebook_password.sendKeys("Rockstar@9");
				facebook_loginbutton.click();
			} else if (context.contains("NATIVE")) {
				driver.context(context);
			}
		}
	}

}
