package com.grabhouse.library;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexMatches
{

	static Pattern r=null;

	public enum TitleRegx{
		PG("(\\A(Sharing|Individual|(Individual\\/Sharing)))\\sPG\\sfor\\s(Ladies|Gents|Ladies&Gents)$"),
		COCOON("(\\A(Sharing|Individual|(Individual\\/Sharing)))\\s([1-6](.5)?\\sBHK|1\\sRK)\\sfor\\s(Ladies|Gents|(Ladies\\s&\\sGents))$"),
		FLAT("(\\A(Fully\\sFurnished|Semi\\sFurnished|Unfurnished))\\s([1-6](.5)?\\sBHK|1\\sRK)\\sflat$");
		private final String pattern;
		TitleRegx(String pattern){
			this.pattern=pattern;
		}
		public String getPattern(){
			return pattern;
		}
	}

	public static boolean verifyTitle(TitleRegx type,String line){
		switch(type){
		case PG:
			r = Pattern.compile(TitleRegx.PG.pattern);
			break;
		case COCOON:
			r = Pattern.compile(TitleRegx.COCOON.pattern);
			break;
		case FLAT:
			r = Pattern.compile(TitleRegx.FLAT.pattern);
			break;
		default:
			System.out.println("Type does not match");
			break;

		}
		Matcher m = r.matcher(line);
		if (m.matches()) {
			return true;
		} else {
			return false;
		}
	}

	public static void main( String args[] ){

		// String to be scanned to find the pattern.
		String line = "Individual/Sharing PG for Gents";
		System.out.println(verifyTitle(TitleRegx.PG, line));

	}
}