package com.grabhouse.library;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

public class WebDriverUtility {

	public static String parentWindow = null;

	public static final String wait="10"; 
	public static List<String> masterCity;
	public static List<String> cocoonCity;
	
	
	public static WebDriver driver = null;
	public static final int implicitWait = Integer.parseInt(wait);
	public WebDriverUtility() {
	}

	public WebDriverUtility(String browser) {
		driver = openBrowser(browser);
	}

	public WebDriver openBrowser(String browser) {
		
		
		return driver;
	}
	/**
	 * This method is to check that element is present or not on a web page
	 * 
	 * @param webelement
	 *            Webelement object to check
	 * @return true/false
	 */

	public static boolean isElementPresent(WebElement webelement) {
		boolean exists = true;
		System.out.println(webelement.toString());
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		try {
			waitForPageToLoad();
			webelement.getTagName();

		} catch (NoSuchElementException e) {
			exists = false;
		} finally {
			driver.manage().timeouts().implicitlyWait(implicitWait, TimeUnit.SECONDS);
		}
		return exists;
	}

	public static boolean isElementPresent(By by) {
		boolean exists = true;
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		try {
			waitForPageToLoad();
			driver.findElement(by);
		} catch (NoSuchElementException e) {
			exists = false;
		} finally {
			driver.manage().timeouts().implicitlyWait(implicitWait, TimeUnit.SECONDS);
		}
		return exists;
	}

	/**
	 * This method is to type the input value in textbox
	 * 
	 * @param element
	 *            Textbox webelement object
	 * @param inputVal
	 *            input value to enter in text box
	 */

	public static void type(WebElement element, String inputVal) {
		element.click();
		element.clear();
		element.sendKeys(inputVal);
	}

	/**
	 * This method is to stop current text for the given time
	 * 
	 * @param sec
	 *            time in sec
	 */

	public static void systemWait(int sec) {
		try {
			Thread.sleep(sec * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * This method is to take screen of the WebPage
	 * 
	 * @param name
	 *            name of the screenshot
	 */

	public static void takeScreenhsot(String name) {
		String outputDir = System.getProperty("user.dir") + File.separator + "Reports" + File.separator + "screenShots";
		File f = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		File saved = new File(outputDir, "ss_" + name + ".png");
		try {
			FileUtils.copyFile(f, saved);
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (saved.getName() != null) {
			Reporter.log("<br><a href=\"../screenShots/" + saved.getName() + "\" target=\"_blank\">" + saved.getName()
					+ "<a><br /><font color=\"black\"/>");
		}
	}

	/**
	 * This method is get all the locations under search
	 * 
	 * @param autosuggest_locations,
	 *            search_location
	 * 
	 * @return location -> WebElement
	 */

	public static WebElement auto_suggest_locations(List<WebElement> autosuggest_locations, String search_location,String type) {
        boolean locatoinDate=false;
		for (WebElement location : autosuggest_locations) {
            
			Actions action = new Actions(driver);
			if(type.equalsIgnoreCase("exact"))
			{
				locatoinDate=location.getText().trim().equals(search_location);
			}
			else
			{
				locatoinDate=location.getText().trim().contains(search_location);
			}
			
			if (locatoinDate) {
				action.moveToElement(location).perform();
				return location;
			}
		}
		throw new RuntimeException("No Returnable/Matching Location");
	}

	/**
	 * This function will handle stale element reference exception
	 * 
	 * @param staledElement
	 *            search div(auto suggest) under search
	 */

	public void handleStaleElement(WebElement staledElement) {
		int count = 0; // It will try 4 times to find same element using name.
		while (count < 4) {
			try {
				// If exception generated that means It Is not able to find
				// element then catch block will handle It.
				// WebElement staledElement =
				// driver.findElement(By.name(elementName));
				// If exception not generated that means element found and
				// element text get cleared.
				staledElement.click();
			} catch (StaleElementReferenceException e) {
				e.toString();
				System.out.println("Trying to recover from a stale element :" + e.getMessage());
				count = count + 1;
			}
			count = count + 4;
		}
	}

	public static WebElement getWebElement(WebDriver driver, String xpath) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
	}

	/**
	 * This function will handle Page to Load
	 * 
	 */

	public static void waitForPageToLoad() {
		String pageLoadStatus = null;
		JavascriptExecutor js;
		do {
			js = (JavascriptExecutor) driver;
			pageLoadStatus = (String) js.executeScript("return document.readyState");
			System.out.print(".");
		} while (!pageLoadStatus.equals("complete"));
		System.out.println();
		System.out.println("Page Loaded.");
	}

	/**
	 * This function will handle Multiple Windows Scenario
	 * 
	 * @param mainWindowHandle
	 *            as openWindowHandle
	 * 
	 */

	public static String getMainWindowHandle() {
		return driver.getWindowHandle();
	}

	public static String getCurrentWindowTitle() {
		String windowTitle = driver.getTitle();
		return windowTitle;
	}

	public static WebDriver switchToNewWindow() {
		Set<String> allWindowHandles = driver.getWindowHandles();
		parentWindow = getMainWindowHandle();
		for (String currentWindowHandle : allWindowHandles) {
			if (!currentWindowHandle.equals(parentWindow)) {
				driver.switchTo().window(currentWindowHandle);
			}
		}
		return driver;
	}

	/**
	 * This function get List of web elements
	 * 
	 * @param by
	 * 
	 */

	public static List<WebElement> getWebElements(By by) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		waitForPageToLoad();
		return wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));
	}

	
	public static WebElement getWebelement(By by)
	{
		WebDriverWait wait=new WebDriverWait(driver, 10);
		waitForPageToLoad();
		return wait.until(ExpectedConditions.presenceOfElementLocated(by));
	}
	public static boolean isChecked(WebElement ele)
	{
		if(ele.getAttribute("checked").equals("true"))
			return true;
		return false;
	}
	
	public static void check(WebElement ele,WebElement statusOfWebelement)
	{
		System.out.println(statusOfWebelement.getAttribute("checked"));

		System.out.println(statusOfWebelement.getAttribute("checked").equals("true"));
		if(!statusOfWebelement.getAttribute("checked").equals("true"))
		{
			ele.click();
		}
	}
	public static void unCheck(WebElement ele,WebElement statusOfWebelement)
	{

		if(statusOfWebelement.getAttribute("checked").equals("true"))
		{
			ele.click();
		}
	}
	
	public static void selectAlternativeRadioButton(List<WebElement> radioButtons)
	{
		for(WebElement ele:radioButtons)
		{
			if(!ele.isSelected())
			{
				ele.click();
				break;
			}
			
		}
	}
	public void handleMultipleWindows(WebDriver driver, String pageTitle) {
		Set<String> windows = driver.getWindowHandles();
		for (String handle : windows) {
			driver.switchTo().window(handle);
			System.out.println(driver.getTitle().toString());
			if (driver.getTitle().equals(pageTitle)) {
				System.out.println(driver.getTitle());
				driver.switchTo().window(handle);
				driver.getCurrentUrl();
			}
		}
	}


}
