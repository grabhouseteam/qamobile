package com.grabhouse.library;


import org.testng.ITestResult;
import org.testng.TestListenerAdapter;



/**
 **
 * Summary: 
 * Implementation of a listener to take screen shots when using web driver for tests that fail. 
 *
 */

public class CustomTestListner extends TestListenerAdapter
{

	@Override
	public void onTestFailure (ITestResult testResult){
   	System.out.println("onTestFailure");
   	System.out.println("==================================================================");
        try {
        	if(SoftAssert.showError){
	          //  String outputDir = testResult.getTestContext().getOutputDirectory();
        	  WebDriverUtility.takeScreenhsot(testResult.getMethod().getMethodName());
	          WebDriverUtility.driver.navigate().refresh();
        	}else{
        		SoftAssert.showError=true;
        	}
        } catch (Exception e) {
        	e.printStackTrace();
			//System.out.println("error generating screenshot: "+e.getStackTrace());
        }
       
		
	}
	@Override
	public void onConfigurationFailure(ITestResult itr){
		  try {
	        	if(SoftAssert.showError){
		          //  String outputDir = testResult.getTestContext().getOutputDirectory();
	        	  System.out.println("Name :"+ itr.getMethod().getMethodName());
		          WebDriverUtility.takeScreenhsot(itr.getMethod().getMethodName());
		          
	        	}else{
	        		SoftAssert.showError=true;
	        	}
	        } catch (Exception e) {
	        	e.printStackTrace();
				//System.out.println("error generating screenshot: "+e.getStackTrace());
	        }
	       
	}
	
	@Override
	public void onTestStart(ITestResult tr){
		System.out.println("**************************************"+tr.getName()+"*****************************");
	}

	@Override
	public void onTestSuccess(ITestResult tr){
		System.out.println("=================================================================================================================");
	}

	@Override
	public void beforeConfiguration(ITestResult tr){
		if(tr.getMethod().isBeforeClassConfiguration()){
			System.out.println("#######################################"+tr.getMethod().getRealClass()+"###########################################");
		}
		
	}
	
} 

