package com.grabhouse.library;

import java.util.Map;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.Reporter;
import org.testng.asserts.Assertion;
import org.testng.asserts.IAssert;
import org.testng.collections.Maps;

public class SoftAssert extends Assertion {
	private Map<AssertionError, IAssert> m_errors = Maps.newHashMap();
	public static boolean showError=true;

	@Override
	public void executeAssert(IAssert a) {
		try {
			a.doAssert();
		} catch (AssertionError ex) {
			onAssertFailure(a, ex);
			Reporter.log("<br><font color=\"red\"/>" + ex.getMessage());
			
			try {
					WebDriverUtility.takeScreenhsot(RandomStringUtils.randomNumeric(4));
				
			} catch (Exception e) {
				System.out.println("error generating screenshot: " + e);
			}
			m_errors.put(ex, a);
		}

	}

	
	
	public void assertAll() {

		if (!m_errors.isEmpty()) {
			showError=false;
			StringBuilder sb = new StringBuilder(
					"The above mentioned asserts failed:\n");

			throw new AssertionError(sb.toString());
		}
	}
}
