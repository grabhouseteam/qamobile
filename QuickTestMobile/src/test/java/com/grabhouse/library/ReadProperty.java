package com.grabhouse.library;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ReadProperty {
	
	public static Properties loadProperties(String fileName) {

		Properties properties = new Properties();
		FileInputStream fis;
		try {
			fis = new FileInputStream(System.getProperty("user.dir") + File.separator + "src/test/resources/capabilities"
					+ File.separator + fileName + ".properties");
		
			properties.load(fis);
		}catch (FileNotFoundException e) {

			e.printStackTrace();
		}
		catch (IOException e) {

			e.printStackTrace();
		}

		return properties;

	}

}