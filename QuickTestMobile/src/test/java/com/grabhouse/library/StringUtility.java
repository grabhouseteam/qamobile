package com.grabhouse.library;

import java.util.Arrays;

import org.apache.commons.lang3.RandomStringUtils;

public class StringUtility {

	/**
	 **
	 * Compare two strings: Expected String, Actual String and return boolean
	 * 
	 */

	public boolean compareString(String toBePresent, String present) {

		if (toBePresent.equals(present)) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 **
	 * Compare a string to be present in an array of strings and return boolean
	 * 
	 */

	public boolean compareSplitedString(String toBeSplited, String toCompare) {

		String str = new String(toBeSplited);
		String[] splitedString = str.split(" ");

		if (Arrays.asList(splitedString).contains(toCompare)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 **
	 * Compare two arrays
	 * 
	 */

	public void compareArrays() {

	}
	/**
	 * random mobile number generator
	 */

	public static String generateRandomMobile()
	{
		String randomNumbers = RandomStringUtils.randomNumeric(9);
		String phNo = 7+randomNumbers;
		return phNo;
	}
	public static String generateRandomString()
	{
		String randomString = RandomStringUtils.randomAlphanumeric(6);
		return randomString;
	}
	
	public static int generateRandomNum(int length,int maxval){
		int returnValue=Integer.parseInt(RandomStringUtils.randomNumeric(length));
		while(returnValue> maxval){
			returnValue=Integer.parseInt(RandomStringUtils.randomNumeric(length));
		}
		return returnValue;
	}
	
	
}
